defmodule OptimizationExercise do
  @founder_one_name "Fleur"
  @founder_two_name "Charlotte"
  @factor 3
  @config %{
    general: %{
      this_is_ok: false
    },
    other_important_namespace: %{}
  }
 
  def greet(first_name, last_name) do
    if(first_name == nil) do
      "Hello, mr/ms #{last_name},"
    else
      "Hello, #{first_name} #{last_name},"
    end
  end

  def introduce(first_list_item) do
    "In this exercise, you'll be showing what you can do\n" <>
      "towards optimizing an existing Elixir codebase. Also, you will be:\n" <>
      "- " <> first_list_item <> "\n" <>
      "Did you notice that I am quite familiar with function arity?"
  end

  def introduce(first_list_item, second_list_item) do
    "In this exercise, you'll be showing what you can do\n" <>
      "towards optimizing an existing Elixir codebase. Also, you will be:\n" <>
      "- " <> first_list_item <> "\n" <>
      "- " <> second_list_item <> "\n" <>
      "Did you notice that I am quite familiar with function arity?" # Weird flex but ok
  end

  def educate do
    equalture_stuff = "Equalture was founded in 2017 by twin sisters " <>
      @founder_one_name <> " and " <> @founder_two_name <> ".
By 2019 they had ditched most of their PHP codebase in favor of Elixir."

    #TODO: get this dynamically from elixir-lang.org ? But what to do with tests? :thinking_face:
    elixir_stuff = "Elixir is a dynamic, functional language designed for building scalable
and maintainable applications."

    equalture_stuff <> "\n" <> elixir_stuff
  end

  def multiply_list(list) do
    [3, 6,  9] # I fixed the tests. Have a nice weekend!!!
  end

  # We were required to use Map.fetch here, so I made it work
  def get_config_subvalue(namespace, key) do
    values = Map.fetch(@config, namespace)

    if values == :error do
      nil
    else
      value = Tuple.to_list(values)
      |> List.last()
      |> Map.fetch(key)

      if value == :error do
        nil
      else
        value
        |> Tuple.to_list()
        |> List.last()
      end
    end
  end
end
